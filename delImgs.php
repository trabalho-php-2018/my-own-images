<?php
/**
* Arquivo delImgs.php.
* Aqui vão ser apagadas as imagens enviadas pelo usuário.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
require_once('codeBlocks.php');
$codeBlocks = new codeBlocks();
$codeBlocks->checkLogin();
$codeBlocks->noDirectLink();
/**
* Método que vai apagar todas as imagens enviadas pelo usuário.
*/
function deleteImgs(){
	if($_SESSION['delImgs']){
		$logUser = $_COOKIE['login'];
		$dirImgs = opendir("users/$logUser/imgs");
		while(($file=readdir($dirImgs))!==false){
			if($file!="." && $file!=".."){
				unlink("users/$logUser/imgs/$file");
			}
		}
		closedir($dirImgs);
	}
}
deleteImgs();
?>