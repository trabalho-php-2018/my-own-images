<?php
	echo "
	<link rel='stylesheet' type='text/css' href='sui/semantic.min.css'>
	<link rel='stylesheet' type='text/css' href='css/own.css'>
	<link rel='icon' href='imgs/iconpag.png'/>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<head><title>MOI - Configurações</title></head>
	<div class='ui vertical inverted sidebar labeled icon menu'>
		<div class='item'><img src='imgs/icon.png'></div>
		<a href='gallery.php' class='item'><i class='file image outline icon'></i>Ver Imagens</a>
		<a href='upload.php' class='item'><i class='ui cloud upload icon'></i>Upload de Imagens</a>
		<a class='item' id='menuConfBut'><i class='cog icon'></i>Configurações da Conta</a>
		<a href='logout.php?noDirectLink=noDirectLink' class='item'><i class='arrow alternate circle down icon'></i>Sair</a>
	</div>
	<div class='pusher'>
";
	require('codeBlocks.php');
	$codeBlocks = new codeBlocks();
	$codeBlocks->checkLogin();
	$codeBlocks->confModal();
	$codeBlocks->fixedMenu();
	$codeBlocks->delImgs('changePwd.php');
	echo "
		<div class='ui three column grid'>
			<div class='ui centered column'>
				<div class='ui form'>
					<form method='POST' action='changePwdBack.php?noDirectLink=true' id='changePwdForm'>
						<fieldset>
							<legend><h2 class='ui teal header'>Trocar senha</h2></legend><br>
							<div class='fields'>
								<div class='four wide field'>
									<div class='ui large teal label fluid vrau changepwd'>Senha Atual:</div>
								</div>
								<div class='six wide field'>
									<input type='password' name='cuPwd' placeholder='Senha atual' required />
								</div>
							</div>
							<div class='ui inverted divider'></div>
							<div class='fields'>
								<div class='four wide field'>
									<div class='ui small teal label fluid vrau' id='newPwdLabel'>Nova Senha:</div>
								</div>
								<div class='six wide field'>
									<input type='password' name='newPwd' id='newPwd' placeholder='Senha nova' required />
									<center>
										<div class='ui hidden label' id='changepwderror'>A senha precisa ter 8 dígitos ou mais.</div>
									 	<div class='ui hidden label' id='changepwderror2'>Para continuar, preencha este campo.</div>
									 </center>
								</div>
								<div class='six wide field'>
									<input type='password' name='cPwd' id='cPwd' placeholder='Confirme a nova senha' required />
									<center>
										<div class='ui hidden label' id='changecpwderror'>As senhas estão diferentes.</div>
										<div class='ui hidden label' id='changecpwderror2'>Para continuar, preencha este campo.</div>
									</center>
								</div>
							</div>
							<div class='ui inverted divider'></div>
							<center>
								<input class='ui inverted teal button' type='submit' value='Trocar Senha' id='changesubmit' />
							</center>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
		<br><br>
		<center><span id='copyright'>©Todos os direitos reservados 2018</span></center>
	</div>
	";
echo "
	<script src='js/jquery.js'></script>
	<script src='js/changePwd.js'></script>
	<script src='sui/semantic.min.js'></script>
	<script type='text/javascript' src='js/own.js'></script>";
?>