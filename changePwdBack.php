<?php
/**
* Arquivo changePwdBack.php.
* Será efetuado a mudança de senha do usuário.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
require_once('codeBlocks.php');
$codeBlocks = new codeBlocks();
$codeBlocks->checkLogin();
$codeBlocks->noDirectLink();
echo "
	<link rel='stylesheet' href='css/own.css'>
	<link rel='stylesheet' href='sui/semantic.min.css'>
";
/**
* Método para habilitar modal em caso de sucesso ou erro na troca de senha do usuário.
* @param boolean $changeModal Vai revelar se a mudança de senha foi um sucesso ou uma falha, e, dependendo da resposta, habilitará seu respectivo modal.
*/
function finalChange($changeModal){
	if(!$changeModal){
		echo "
			<script>
				$('#errorChangeModal').modal('setting', 'closable', false).modal('show');
			</script>";
	}else{
		echo "
			<script>
				$('#successChangeModal').modal('setting', 'closable', false).modal('show');
			</script>";
	}
}
/**
* Método para efetuar a mudança de senha do usuário.
* @return boolean
*/
function changePassword(){
	$cuPwd = $_POST['cuPwd'];
	$newPwd = $_POST['newPwd'];
	$cPwd = $_POST['cPwd'];
	if(strlen($newPwd)>=8 && $newPwd === $cPwd){
		$userArqs = $_COOKIE['login'];
		$pwdFile = fopen("users/$userArqs/pwd.txt", "r");
		$a = fread($pwdFile, filesize("users/$userArqs/pwd.txt"));
		$passwords = explode("<|>", $a);
		array_pop($passwords);
		fclose($pwdFile);
		$b = count($passwords);
		if($cuPwd === $passwords[$b-1]){
			$exist;
			if(in_array($newPwd,$passwords)){
				$exist=true;
			}
			if(!isset($exist)){
				unlink("users/$userArqs/pwd.txt");
				if(count($passwords)==3){
					array_shift($passwords);
				}
				array_push($passwords, $newPwd);
				$newPwdFile = fopen("users/$userArqs/pwd.txt", "a");
				for($i=0;$i<count($passwords);$i++){
					fwrite($newPwdFile, "{$passwords[$i]}<|>");		
				}
				fclose($newPwdFile);
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}else{
		return false;
	}
}
echo "
	<div class='ui tiny inverted modal' id='successChangeModal'>
		<div class='ui icon header'>
			<i class='thumbs up icon'></i>
			Mudança de senha realizada com sucesso!
		</div>
		<div class='content'>
			<p><span style='color:green'>Sucesso!</span>Sua mudança de senha foi realizada, clique no botão abaixo para voltar para a página inicial.</p>
		</div>
		<div class='actions'>
			<a href='index.php' class='ui ok green button'>Ok</a>
		</div>
	</div>
	<div class='ui tiny inverted modal' id='errorChangeModal'>
		<div class='ui icon header'>
	    	<i class='thumbs down icon'></i>
	    	Erro ao efetuar a mudança de senha!
	  	</div>
	  	<div class='content'>
	    	<p><span style='color:red'>Algo deu errado!</span>Para tentar resolver o problema, verifique se a senha atual está correta, se a nova senha é diferente das três últimas usadas por você e  se a senha nova e sua confirmação estão iguais.</p>
	  	</div>
	  	<div class='actions'>
	    	<a href='changePwd.php' class='ui ok green button'>Ok</a>
	  	</div>
	</div>
	<script src='js/jquery.js'></script>
	<script src='sui/semantic.min.js'></script>";
$successChange = changePassword();
finalChange($successChange);
?>