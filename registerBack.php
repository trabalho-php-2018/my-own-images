<?php
/**
* Arquivo registerBack.php.
* Usado para realizar o cadastro de um usuário.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
/**
* Método para habilitar o modal de sucesso ou erro do registro.
* @param boolean $eM Vai revelar se o cadastro foi um sucesso ou mal-sucedido.
*/
function finalReg($eM){
	if(!$eM){
		echo "
			<script>
				$('#errorRegModal').modal('setting', 'closable', false).modal('show');
			</script>";
	}else{
		echo "
			<script>
				$('#successRegModal').modal('setting', 'closable', false).modal('show');
			</script>";
	}
}
/**
* Método para efetuar cadastro.
* @param boolean $noDLReg Vai permitir ou não a ocorrência do login para garantir que o mesmo seja sendo efetuado sem o uso de um link direto.
* @return boolean
*/
function registerUser($noDLReg){
	if($noDLReg){	
		$regUser = addslashes($_POST['reguser']);
		$regPwd = addslashes($_POST['regpwd']);
		$regCpwd = addslashes($_POST['regcpwd']);
		$regEmail = addslashes($_POST['regemail']);
		if(strlen($regUser)>0 && strlen($regUser)<=16 && strlen($regPwd)>=8 && $regPwd===$regCpwd){
			if(!file_exists("users/$regUser")){
				if($regPwd===$regCpwd){
					mkdir("users/$regUser", 0777);
					mkdir("users/$regUser/imgs", 0777);
					$arq = fopen("users/$regUser/pwd.txt", "w");
					fwrite($arq, "$regPwd<|>");
					fclose($arq);
					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
echo "	
	<div class='ui tiny modal' id='errorRegModal'>
		<div class='ui icon header'>
	    	<i class='thumbs down icon'></i>
	    	Erro ao efetuar cadastro!
	  	</div>
	  	<div class='content'>
	    	<p><span style='color:red'>Algo deu errado!</span>Para tentar resolver o problema, tente inserir outro nome de usuário e confira se a senha e sua confirmação batem.</p>
	  	</div>
	  	<div class='actions'>
	    	<a href='index.php' class='ui ok green button'>Ok</a>
	  	</div>
	</div>
	<div class='ui tiny modal' id='successRegModal'>
		<div class='ui icon header'>
			<i class='thumbs up icon'></i>
			Cadastro realizado com sucesso!
		</div>
		<div class='content'>
			<p><span style='color:green'>Sucesso!</span> Seu cadastro foi realizado, clique no botão abaixo para continuar e fazer o login.</p>
		</div>
		<div class='actions'>
			<a href='index.php' class='ui ok green button'>Ok</a>
		</div>
	</div>
	<script src='js/jquery.js'></script>
	<script src='sui/semantic.min.js'></script>";
$sucessReg = registerUser($noDLReg);
finalReg($sucessReg);
?>