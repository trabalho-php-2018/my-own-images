<?php
	echo "
	<link rel='stylesheet' href='sui/semantic.min.css'/>
	<link rel='stylesheet' href='css/wowslider.css'/>
	<link rel='icon' href='imgs/iconpag.png'/>
	<link rel='stylesheet' href='css/own.css'/>
	<meta name='viewport' content='width=device-width, initial-scale=1'>
	<head><title>MOI - Imagens</title></head>
	<div class='ui vertical inverted sidebar labeled icon menu'>
		<div class='item'><img src='imgs/icon.png'></div>
		<a href='#' class='active item'><i class='file image outline icon'></i>Ver Imagens</a>
		<a href='upload.php' class='item'><i class='ui cloud upload icon'></i>Upload de Imagens</a>
		<a class='item' id='menuConfBut'><i class='cog icon'></i>Configurações da Conta</a>
		<a href='logout.php?noDirectLink=noDirectLink' class='item'><i class='arrow alternate circle down icon'></i>Sair</a>
	</div>
	<div class='pusher'>
	";
	require_once('codeBlocks.php');
	$codeBlocks = new codeBlocks();
	$codeBlocks->checkLogin();
	$codeBlocks->confModal();
	$codeBlocks->fixedMenu();
	$codeBlocks->delImgs('gallery.php');
	require_once("slider.php");
	echo "
	<center><span id='copyright'>©Todos os direitos reservados 2018</span></center>
	</div>
	<script type='text/javascript' src='js/jquery.js'></script>
	<script type='text/javascript' src='sui/semantic.min.js'></script>
    <script type='text/javascript' src='js/wowslider.js'></script>
    <script type='text/javascript' src='js/own.js'></script>
    <script>
        jQuery('#wowslider-container').wowSlider({effect:'rotate',prev:'',next:'',duration:20*100,delay:20*100,width:580,height:212,autoPlay:true,stopOnHover:false,loop:false,bullets:true,caption:true,captionEffect:'slide',controls:true,logo:'',images:0});
    </script>
	";
?>