<?php
/**
* Arquivo loginBack.php.
* Usado para realizar o login de um usuário.
* @author Rodrigo da Silva Freitas <rodrigojato@hotmail.com>
* @author Bruno de Paiva Cruz <bruno.zpc@hotmail.com>
* @author Lucas Silva Rocha <draketmj@gmail.com>
* @author Gabriel Freire Ribeiro <bielfreireoficial@gmail.com>
* @author Rian Gomes Marinho <riangmarinho@gmail.com>
* @package trabalho
*/
/**
* Método para habilitar o modal de erro do login.
* @param boolean $eM Vai revelar se existe a necessidade de habilitar o modal ou não.
*/
function finalLog($eM){
	if(!$eM){
		echo "
			<script>
				$('#errorLogModal').modal('setting', 'closable', false).modal('show');
			</script>";
	}else{
		header("location: index.php");
	}
}
/**
* Método para efetuar login.
* @param boolean $noDLLog Vai permitir ou não a ocorrência do login para garantir que o mesmo seja sendo efetuado sem o uso de um link direto.
* @return boolean
*/
function loginUser($noDLLog){
	if($noDLLog)
		$logUser = $_POST['loguser'];
		$logPwd = $_POST['logpwd'];
		if(file_exists("users/$logUser")){
			$pwdFile = fopen("users/$logUser/pwd.txt", "r");
			$a = fread($pwdFile, filesize ("users/$logUser/pwd.txt"));
			$passwords = explode("<|>", $a);
			array_pop($passwords);
			if($logPwd === $passwords[count($passwords)-1]){
				setcookie("login", "$logUser", time()+ 7200);
				return true;
			}else{
				return false;
			}
			fclose($pwdFile);
		}else{
			return false;
		}
}
echo "
	<div class='ui tiny modal' id='errorLogModal'>
	  <div class='ui icon header'>
	    <i class='thumbs down icon'></i>
	    Erro ao efetuar login!
	  </div>
	  <div class='content'>
	    <p><span style='color:red'>Algo deu errado!</span> Confira seu usuário e sua senha. Caso não tenha se cadastrado ainda, faça isso abaixo.</p>
	  </div>
	  <div class='actions'>
	    <a href='index.php' class='ui ok green button'>Ok</a>
	  </div>
	</div>
	<script src='js/jquery.js'></script>
	<script src='sui/semantic.min.js'></script>";
$successLog = loginUser($noDLLog);
finalLog($successLog);

?>